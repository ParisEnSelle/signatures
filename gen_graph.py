# coding: utf-8
import os
import MySQLdb as db
import matplotlib as mpl
import pandas as pd

DB_HOST = os.environ.get("DB_HOST", None)
DB_USER = os.environ.get("DB_USER", None)
DB_PASSWORD = os.environ.get("DB_PASSWORD", None)
DB_NAME = os.environ.get("DB_NAME", None)

mpl.rcParams['figure.figsize'] = (10,10)
mpl.use('Agg')

conn = db.connect(host=DB_HOST, user=DB_USER, passwd=DB_PASSWORD, db=DB_NAME)

df_confirmed=pd.read_sql('SELECT date FROM wp_dk_speakout_signatures WHERE is_confirmed = "1";', con=conn)
df_unconfirmed=pd.read_sql('SELECT date FROM wp_dk_speakout_signatures WHERE is_confirmed = "0";', con=conn)

dateindex_confirmed = pd.DatetimeIndex(df_confirmed['date'])
dateindex_unconfirmed = pd.DatetimeIndex(df_unconfirmed['date'])

values_confirmed = len(df_confirmed)
values_unconfirmed = len(df_unconfirmed)

confirmed = pd.Series(list(range(values_confirmed)), index=dateindex_confirmed)
unconfirmed = pd.Series(list(range(values_unconfirmed)), index=dateindex_unconfirmed)

plot = confirmed.plot(label='Signatures confirmées ({})'.format(values_confirmed)).legend()
fig = plot.get_figure()
fig.savefig('./confirmed_signatures.png')
plot = unconfirmed.plot(label='Signatures non confirmées ({})'.format(values_unconfirmed)).legend()
fig = plot.get_figure()
fig.savefig('./all_signatures.png')
